import axios from "axios";
import React from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2099/register/login",
        {
          email: email,
          password: password,
        }
      );

      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        history.push("/home");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon, setPasswordIcon] = useState("fa-solid fa-eye-slash");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye");
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash");
  };
  return (
    <section className="relative flex flex-wrap lg:h-screen lg:items-center">
      <div className="w-full px-4 py-12 sm:px-6 sm:py-16 lg:w-1/2 lg:px-8 lg:py-24">
        <div className="mx-auto max-w-lg text-center">
          <h1 className="text-2xl font-bold sm:text-3xl">Ayo Login!</h1>

         
        </div>

        <form
          onSubmit={login}
          action=""
          className="mx-auto mb-0 mt-8 max-w-md space-y-4"
        >
          <div>
            <label for="email" className="sr-only">
              Email
            </label>

            <div className="relative">
              <input
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Masukkan email"
              />

              <span className="absolute inset-y-0 end-0 grid place-content-center px-4">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                  />
                </svg>
              </span>
            </div>
          </div>

          <div>
            <label for="password" className="sr-only">
              Password
            </label>

            <div className="relative">
              <div className="relative">
                <input
                  value={password}
                  type={passwordType}
                  className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                  placeholder="Masukkan password"
                  onChange={(e) => setPassword(e.target.value)}
                />

                <span
                  onClick={togglePassword}
                  className="absolute inset-y-0 text-gray-400 right-0 grid place-content-center px-4"
                >
                  <i class={passwordIcon}></i>
                </span>
              </div>
            </div>
          </div>

          <div className="flex items-center justify-between">
            <p className="text-sm text-gray-500">
              Belum punya akun?
              <a className="underline" href="/register">
                Register
              </a>
            </p>

            <button
              type="submit"
              className="inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
            >
              Login
            </button>
          </div>
        </form>
      </div>

      <div className="relative h-64 w-full sm:h-96 lg:h-full lg:w-1/2">
        <img
          alt="Welcome"
          src="https://i.pinimg.com/564x/b1/dd/36/b1dd364120641d0981ca8f070abf02fe.jpg"
          className="absolute inset-0 h-full w-full object-cover"
        />
      </div>
    </section>
  );
}
