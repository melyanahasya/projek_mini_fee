import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon, setPasswordIcon] = useState("fa-solid fa-eye-slash");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye");
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash");
  };

  const history = useHistory();

  const register = async (e) => {
    e.preventDefault();
    try {
      await axios
        .post("http://localhost:2099/register/register", {
          email: email,
          username: username,
          password: password,
        })
        .then(() => {
          Swal.fire({
            icon: "success",
            title: "Berhasil Registrasi",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            history.push("/");
            window.location.reload();
          }, 1500);
        });
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };
  return (
    <section className="relative flex flex-wrap lg:h-screen lg:items-center">
      <div className="w-full px-4 py-12 sm:px-6 sm:py-16 lg:w-1/2 lg:px-8 lg:py-24">
        <div className="mx-auto max-w-lg text-center">
          <h1 className="text-2xl font-bold sm:text-3xl">Silahkan Register !</h1>

        
        </div>

        <form
          onSubmit={register}
          action=""
          className="mx-auto mb-0 mt-8 max-w-md space-y-4"
        >
          <div>
            <label for="email" className="sr-only">
              Username
            </label>

            <div className="relative">
              <input
                onChange={(e) => setUsername(e.target.value)}
                type="username"
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Masukkan Username"
              />

              <span className="absolute inset-y-0 end-0 grid place-content-center px-4">
              <i className="fa-solid fa-user text-gray-400"></i>
              </span>
            </div>
          </div>
          <div>
            <label for="email" className="sr-only">
              Email
            </label>

            <div className="relative">
              <input
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Masukkan Email"
              />

              <span className="absolute inset-y-0 end-0 grid place-content-center px-4">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                  />
                </svg>
              </span>
            </div>
          </div>

          <div>
            <label for="password" className="sr-only">
              Password
            </label>

            <div className="relative">
              <input
                value={password}
                type={passwordType}
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm "
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />

              <span
                onClick={togglePassword}
                className="absolute text-gray-400 inset-y-0 right-0 grid place-content-center px-4"
              >
                   <i class={passwordIcon}></i>
              </span>
            </div>
          </div>

          <div className="flex items-center justify-between">
            <button
              type="submit"
              className="inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
            >
              Register
            </button>
          </div>
        </form>
      </div>

      <div className="relative h-64 w-full sm:h-96 lg:h-full lg:w-1/2">
        <img
          alt="Welcome"
          src="https://i.pinimg.com/564x/a7/56/7c/a7567c32cd8ad3244eddda3ff6cdacac.jpg"
          className="absolute inset-0 h-full w-full object-cover"
        />
      </div>
    </section>
  );
}
