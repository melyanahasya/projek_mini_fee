import React from "react";
import Swal from "sweetalert2";
import { useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import axios from "axios";
import { useEffect } from "react";

export default function EditProfil() {
  const [alamat, setAlamat] = useState("");
  const [tanggalLahir, setTtl] = useState("");
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [nama, setNama] = useState("");
  const [show, setShow] = useState(false);

  useEffect(() => {
    axios
      .get("http://localhost:2099/register/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        setNama(profil.username);
        setEmail(profil.email);
        setAlamat(profil.alamat);
        setTtl(profil.tanggalLahir);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const Put = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        `http://localhost:2099/register/` + localStorage.getItem("userId"),
        {
          nama: nama,
          email: email,
          alamat: alamat,
          tanggalLahir: tanggalLahir,
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/profil");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="mt-[3rem] ml-[25rem]">
      <form
        className="space-y-3 md:pl-9 pl-[4.5rem] shadow-lg border-t-violet-300 border-t-4 w-[34.5rem]"
        onSubmit={Put}
      >
        <h3 className="md:py-7 py-4 md:text-2xl text-xl font-medium text-black dark:text-black">
          Edit Profil
        </h3>

        <div className="md:flex gap-3">
          <div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Nama
              </label>
              <input
                placeholder="Nama"
                onChange={(e) => setNama(e.target.value)}
                value={nama}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Email
              </label>
              <input
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Alamat
              </label>
              <input
                placeholder="Alamat"
                onChange={(e) => setAlamat(e.target.value)}
                value={alamat}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Tanggal Lahir
              </label>
              <input
                placeholder="Tanggal Lahir"
                onChange={(e) => setTtl(e.target.value)}
                value={tanggalLahir}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
                type="date"
              />
            </div>

            <button
              type="submit"
              className="mb-5 w-[5rem] md:ml-0 ml-[2rem] h-8 md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Simpan
            </button>
            <a
              href="/profile"
              className="w-[5rem] md:mr-5 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 h-8 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
            >
              Batal
            </a>
          </div>
        </div>
      </form>
    </div>
  );
}
