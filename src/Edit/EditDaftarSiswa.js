import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function EditDaftarSiswa() {
  const [show, setShow] = useState(false);
  const history = useHistory();
  const param = useParams();
  const [username, setUsername] = useState("");
  const [kelas, setKelas] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempat, setTempat] = useState("");
  const [alamat, setAlamat] = useState("");


  const Put = async (e) => {
      e.preventDefault();
      e.persist();
  
      try {
        await axios.put(
          `http://localhost:2099/pasien/${param.id}_siswa`, {
          username: username,
          kelas: kelas,
          tempat: tempat,
          tanggalLahir: tanggalLahir,
          alamat:alamat,
          jabatan: kelas,
      });
      setShow(false);
        Swal.fire({
          icon: "success",
          title: "Berhasil Mengedit",
          showConfirmButton: false,
          timer: 1500,
        });
      setTimeout(() => {
        history.push("/daftarSiswa");
        window.location.reload();
      }, 1500);
      } catch (err) {
        console.log(err);
      }
    };

    useEffect(() => {
      axios
        .get("http://localhost:2099/pasien/" + param.id)
        .then((response) => {
          const siswa = response.data.data;
          setUsername(siswa.username);
          setKelas(siswa.kelas);
          setTanggalLahir(siswa.tanggalLahir);
          setTempat(siswa.tempat);
          setAlamat(siswa.alamat);
        })
        .catch((error) => {
          alert("Terjadi Kesalahan " + error);
        });
    }, [param.id]);


  return (
    <div className="mt-[2rem] ml-[25rem]">
      <form
        className="space-y-3 md:pl-9 pl-[4.5rem] shadow-lg border-t-violet-300 border-t-4 w-[34.5rem]"
        onSubmit={Put}
      >
        <h3 className="md:py-5 md:text-2xl text-xl font-medium text-black dark:text-black">
          Edit Daftar Siswa
        </h3>

        <div className="md:flex gap-3">
          <div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Siswa
              </label>
              <input
                placeholder="Nama Siswa"
                onChange={(e) => setUsername(e.target.value)}
                value={username}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Kelas
              </label>
              <input
                placeholder="Kelas"
                onChange={(e) => setKelas(e.target.value)}
                value={kelas}
                className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Tempat Lahir
              </label>
              <input
                placeholder="Tempat Lahir"
                onChange={(e) => setTempat(e.target.value)}
                value={tempat}
                className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Tanggal Lahir
              </label>
              <input
                placeholder="Tanggal Lahir"
                onChange={(e) => setTanggalLahir(e.target.value)}
                value={tanggalLahir}
                type="date"
                className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Alamat
              </label>
              <input
                placeholder="Alamat"
                onChange={(e) => setAlamat(e.target.value)}
                value={alamat}
                className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>

            <button
              type="submit"
              className="mb-5 w-[5rem] md:ml-0 ml-[2rem] h-8 md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Simpan
            </button>
            <a
              href="/daftarSiswa"
              className="w-[5rem] md:mr-5 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 h-8 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
            >
              Batal
            </a>
          </div>
        </div>
      </form>
    </div>
  );
}
