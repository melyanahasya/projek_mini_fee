import React from "react";
import Sidebar from "../Component/Sidebar";
import { useHistory, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import axios from "axios";

export default function StatusPeriksa() {
  const [username, setUsername] = useState("");
  const [statusPasien, setStatusPasien] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const history = useHistory();
  const param = useParams();
  const [show, setShow] = useState(false);
  const [penyakitId, setPenyakitId] = useState("");
  const [tanganiId, setTanganiId] = useState("");
  const [PenangananId, setPenangananId] = useState("");
  const [periksaPasien, setPeriksaPasien] = useState({
    username: "",
    status: "",
    keluhan: "",
  });
  const [listDiagnosa, setListDiganosa] = useState([]);
  const [listPenanganan, setListPenanganan] = useState([]);
  const [listTindakan, setListTindakan] = useState([]);
  const [pages, setPages] = useState(0);
  const [tangani, setTangani] = useState("sudah ditangani");

  const getAllPenanganan = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/penanganan_pertama/all-penanganan?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListPenanganan(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllTindakan = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/tindakan/all-tindakan?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListTindakan(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllDiagnosa = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/diagnosa/all-diagnosa?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListDiganosa(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  // const Put = async (e) => {
  //   e.preventDefault();
  //   e.persist();

  //   try {
  //     await axios.put(
  //       (`http://localhost:1212/periksa_pasien/${param.id}?keluhan=${keluhan}&panyakitId=${penyakitId}&penangananId=${PenangananId}&status=${tangani}&tindakanId=${tanganiId}`), {
  //   });
  //   setShow(false);
  //     Swal.fire({
  //       icon: "success",
  //       title: "Berhasil Ditambah",
  //       showConfirmButton: false,
  //       timer: 1500,
  //     });
  //   setTimeout(() => {
  //     history.push("/periksaPasien");
  //     window.location.reload();
  //   }, 1500);
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  useEffect(() => {
    axios
      .get("http://localhost:2099/periksa_pasien/" + param.id)
      .then((response) => {
        const tangani = response.data.data;
        setUsername(tangani.namaPasien.username);
        setStatusPasien(tangani.statusPasien);
        setKeluhan(tangani.keluhan);
      })
      .catch((error) => {
        // alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  useEffect(() => {
    getAllDiagnosa(0);
    getAllPenanganan(0);
    getAllTindakan(0);
  }, []);
  return (
    <div>
      <Sidebar />
      <div className="ml-[18rem] mt-[5rem] pr-8">
        <div className="flex bg-violet-200">
          <p className="pl-6 h-10 p-2 text-xl">Periksa Pasien : {username}</p>
        </div>
        <div className="border">
          <div className="flex gap-10">
            <div>
              <label className="mt-5 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Pasien
              </label>
              <p
                className="bg-gray-50 ml-5 md:w-[29rem] h-9 rounded border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              >
                {username}
              </p>
            </div>
            <div>
              <label className="mt-5 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Status Pasien
              </label>
              <p
                className="bg-gray-50 ml-5 md:w-[29rem] h-9 rounded border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              >
                {statusPasien}
              </p>
            </div>
          </div>
          <div>
            <label className="mt-2 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
              Keluhan Pasien
            </label>
            <textarea
              className="bg-gray-50 ml-5 md:w-[61.9rem] h-12 rounded border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            >
              {keluhan}
            </textarea>
          </div>
          <div>
            <div className="flex gap-4">
              <div>
                <label className="mt-2 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                  Penyakit Pasien
                </label>
                {/* <p
                  className="bg-gray-50 ml-5 md:w-[12rem] h-9 rounded border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                >
                  Pusing
                </p> */}

                <select
                  className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  name="whatever"
                  id="frm-whatever"
                  onChange={(e) => setPenyakitId(e.target.value)}
                >
                  <option selected>Pilih Penyakit</option>
                  {listDiagnosa.map((data, index) => {
                    return (
                      <option key={index} value={data.id}>
                        {data.namaDiagnosa}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div>
                <label className="mt-2 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                  Penanganan Pertama
                </label>

                <select
                  className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  name="whatever"
                  id="frm-whatever"
                  onChange={(e) => setPenangananId(e.target.value)}
                >
                  <option selected>Pilih Penyakit</option>
                  {listDiagnosa.map((data, index) => {
                    return (
                      <option key={index} value={data.id}>
                        {data.namaDiagnosa}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div>
                <label className="mt-2 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                  Tindakan
                </label>
                <select
                  className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  name="whatever"
                  id="frm-whatever"
                  onChange={(e) => setTanganiId(e.target.value)}
                >
                  <option selected>Pilih Tindakan</option>
                  {listTindakan.map((data, index) => {
                    return (
                      <option key={index} value={data.id}>
                        {data.namaTindakan}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div>
                <label className="mt-2 ml-5 block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                  Catatan
                </label>
                <input
                  placeholder="Catatan"
                  className="bg-white border border-gray-300 h-8 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                  onChange={(e) => setKeluhan(e.target.value)}
                  value={keluhan}
                />
              </div>
              <div>
                <button
                  data-modal-target="authentication-modal"
                  data-modal-toggle="authentication-modal"
                  className="md:text-base md:mt-11 w-[4rem] md:w-[5.5rem] md:ml-2 text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                  type="submit"
                  // onClick={handleShow}
                >
                  Tambah
                </button>
              </div>
            </div>
          </div>

          <table className="min-w-[35rem] ml-5 pr-5 mb-3 border shadow-lg mt-4 text-center">
            <thead className="border-b">
              <tr>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium text-gray-900 md:w-9 md:py-4 py-2 border-r"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
                >
                  Nama Pasien
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
                >
                  Tindakan
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
                >
                  Catatan
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
                >
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="border-b">
                <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                  1
                </td>
                <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                  Ibu Depra
                </td>
                <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                  Operasi
                </td>
                <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                  tidak ada catatan apa apa
                </td>
                <td className="md:text-sm text-xs ml-[3rem] text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap flex gap-3 border-r">
                  -
                </td>
              </tr>
            </tbody>
          </table>

          <button
            data-modal-target="authentication-modal"
            data-modal-toggle="authentication-modal"
            className="md:text-base md:mt-3 w-[4rem] md:w-[5.5rem] mb-3 md:ml-[58rem] text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
            type="submit"
            // onClick={handleShow}
          >
            Selesai
          </button>
        </div>
      </div>
    </div>
  );
}
