import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function EditDaftarObat() {
  const [show, setShow] = useState(false);
  const param = useParams();
  const history = useHistory();

  const [namaObat, setNamaObat] = useState("");
  const [stok, setStok] = useState("");

  const put = async (e) => {
    e.preventDefault();

    try {
      await axios.put("http://localhost:2099/daftar_obat/" + param.id, {
        namaObat: namaObat,
        stok: stok,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/daftarObat");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:2099/daftar_obat/" + param.id)
      .then((response) => {
        const obat = response.data.data;
        setNamaObat(obat.namaObat);
        setStok(obat.stok);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div className="mt-[5rem] ml-[25rem]">
      <form
        className="space-y-3 md:pl-9 pl-[4.5rem] shadow-lg border-t-violet-300 border-t-4 w-[34.5rem]"
        onSubmit={put}
      >
        <h3 className="md:mb-4 md:py-7 py-4 md:text-2xl text-xl font-medium text-black dark:text-black">
          Edit Obat
        </h3>

        <div className="md:flex gap-3">
          <div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Obat
              </label>
              <input
                placeholder="Nama Obat"
                onChange={(e) => setNamaObat(e.target.value)}
                value={namaObat}
                className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Stok
              </label>
              <input
                placeholder="Stok Obat"
                onChange={(e) => setStok(e.target.value)}
                value={stok}
                className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>

            <button
              type="submit"
              className="mb-5 w-[5rem] md:ml-0 ml-[2rem] h-8 md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Simpan
            </button>
            <a
              href="/daftarObat"
              className="w-[5rem] md:mr-5 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 h-8 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
            >
              Batal
            </a>
          </div>
        </div>
      </form>
    </div>
  );
}
