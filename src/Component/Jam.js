import React, { useState } from "react";
import moment from "moment";

export default function Jam() {

  let time = new Date().toLocaleTimeString();
  const [currentTime, setCurrentTime] = useState(time);

  const updateTime = () => {
    let time = new Date().toLocaleTimeString();
    setCurrentTime(time);
  };

  setInterval(updateTime, 1000);

  const dateTime = new Date();

  return (
    <div>
      <p>{moment(dateTime).format("ddd DD/MM/YYYY")}</p>
      <p>{currentTime}</p>
    </div>
  );
}
