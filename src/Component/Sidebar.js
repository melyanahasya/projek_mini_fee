import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import Jam from "./Jam";
import axios from "axios";
import Swal from "sweetalert2";

export default function Sidebar() {
  const [open, setOpen] = useState(false);

  const active = "bg-gray-500 text-black";
  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");
  const refreshPage = () => {
    window.location.reload();
  };

  var mini = true;

  const [isOpen, setIsOpen] = useState(false);

  const [profile, setProfile] = useState({
    foto: null,
  });

  const getAll = async () => {
    await axios
      .get("http://localhost:2099/register/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const history = useHistory();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };
  return (
    <div>
      {/* Navbar */}
      <nav className="fixed top-0 shadow-lg z-50 w-full bg-white border-b border-white dark:bg-gray-800 dark:border-gray-700">
        <div className="px-3 py-3 lg:px-5 lg:pl-3">
          <div className="flex items-center justify-between">
            <div className="flex mr-5 items-center justify-start">
              <button
                data-drawer-target="logo-sidebar"
                data-drawer-toggle="logo-sidebar"
                aria-controls="logo-sidebar"
                type="button"
                className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg sm:hidden hover:bg-gray-500 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              >
                <span className="sr-only">Open sidebar</span>
              </button>
              <p className="flex md:mr-24">
                <span className="self-center text-xl font-semibold sm:text-2xl whitespace-nowrap dark:text-white">
                  SISTEM APLIKASI UKS
                </span>
              </p>
            </div>

            {/* profil */}
            <div className="flex items-center">
              <div className="flex items-center ml-[30rem]">
                <div>
                  <a
                    href="/profile"
                    type="button"
                    className="flex text-sm bg-gray-800 rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600"
                    aria-expanded="false"
                    data-dropdown-toggle="dropdown-user"
                  >
                    <span className="sr-only">Open user menu</span>
                    {profile.foto ? (
                      <img
                        src={profile.foto}
                        alt=""
                        className="rounded-full max-w-[2rem] h-[2rem] w-[2.5rem] "
                      />
                    ) : (
                      <img
                        src="https://i.pinimg.com/736x/7b/3a/37/7b3a37aaa3c89d4a45a70d40ece74271.jpg"
                        alt=""
                        className="rounded-full max-w-[2rem] h-[2rem] w-[2.5rem] "
                      />
                    )}
                  </a>

                  {/* <div className="flex gap-2 ml-auto md:ml-auto pr-4">
                    {profile.foto ? (
                      <a href="/profil">
                        <img
                          className=" mt-2 rounded-full max-w-[2rem] h-[2rem] w-[2.5rem] border-black border"
                          src={profile.foto}
                          alt=""
                        />
                      </a>
                    ) : (
                      <a href="/profile">
                        <img
                          className=" mt-2 rounded-full max-w-[2rem] h-[2rem] w-[2.5rem] border-black border"
                          src="https://static.vecteezy.com/system/resources/previews/008/442/086/original/illustration-of-human-icon-user-symbol-icon-modern-design-on-blank-background-free-vector.jpg"
                          alt=""
                        />
                      </a>
                    )}
                    <p className="mt-3 md:mt-3 text-base md:text-base">
                      {localStorage.getItem("role")}
                    </p>
                  </div> */}
                </div>
                {/* <div
                  className="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600"
                  id="dropdown-user"
                >
                  <div className="px-4 py-3" role="none">
                    <p
                      className="text-sm text-gray-900 dark:text-white"
                      role="none"
                    >
                      Neil Sims
                    </p>
                    <p
                      className="text-sm font-medium text-gray-900 truncate dark:text-gray-300"
                      role="none"
                    >
                      neil.sims@flowbite.com
                    </p>
                  </div>
                  <ul className="py-1" role="none">
                    <li>
                      <a
                        href="#"
                        className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-500 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white"
                        role="menuitem"
                      >
                        Dashboard
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-500 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white"
                        role="menuitem"
                      >
                        Settings
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-500 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white"
                        role="menuitem"
                      >
                        Earnings
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-500 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white"
                        role="menuitem"
                      >
                        Sign out
                      </a>
                    </li>
                  </ul>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </nav>

      <aside
        id="logo-sidebar"
        className="fixed top-0 left-0 z-40 w-64 h-screen pt-20 transition-transform -translate-x-full bg-gray-800 border-r border-gray-200 sm:translate-x-0 dark:bg-gray-800 dark:border-gray-700"
        aria-label="Sidebar"
      >
        <div className="h-full px-3 pb-4 overflow-y-auto bg-gray-800 dark:bg-gray-700">
          <ul className="space-y-2 font-medium">
            <li>
              <a
                href="/dashboard"
                className="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-gauge text-white"></i>
                <span className="ml-3">Dashboard</span>
              </a>
            </li>
            <li>
              <a
                href="periksaPasien"
                className="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-stethoscope text-white"></i>
                <span className="flex-1 ml-3 whitespace-nowrap">
                  Periksa Pasien
                </span>
              </a>
            </li>

            <div>
              <div
                onClick={() => setIsOpen((prev) => !prev)}
                className="flex cursor-pointer font-medium text-sm items-center md:gap-4 md:mt-1 rounded-lg py-2 text-black hover:bg-gray-500 md:p-2 hover:text-gray-700"
              >
                <i className="fa-solid fa-server text-white"></i>
                <span className={`${open && "hidden"}`}>
                  {" "}
                  <p className="text-white">Data</p>{" "}
                </span>
                {!isOpen ? (
                  <i
                    className={`fas fa-caret-down transition ml-[7.5rem] text-white duration-300 rotate-90`}
                  ></i>
                ) : (
                  <i
                    className={`fas fa-caret-down transition ml-[7.5rem] text-white duration-300 rotate-0`}
                  ></i>
                )}
              </div>
              {isOpen && (
                <div className="relative block ml-3 rounded-lg p-2 w-full">
                  <a
                    className="flex items-center gap-2 rounded-lg py-2 text-black hover:bg-gray-500 md:p-2 hover:text-gray-700"
                    href="/daftarGuru"
                  >
                    <i
                      className={`fa-regular fa-circle  text-white ${
                        open && "hidden"
                      }`}
                    ></i>
                    <span className={`text-sm font-medium ${open && "hidden"}`}>
                      <p className="text-white">Daftar Guru</p>
                    </span>
                  </a>
                  <a
                    className="flex items-center gap-2 rounded-lg py-2 text-black hover:bg-gray-500 md:p-2 hover:text-gray-700"
                    href="/daftarSiswa"
                  >
                    <i
                      className={`fa-regular fa-circle text-white ${
                        open && "hidden"
                      }`}
                    ></i>
                    <span className={`text-sm font-medium ${open && "hidden"}`}>
                      <p className="text-white"> Daftar Siswa</p>
                    </span>
                  </a>
                  <a
                    className="flex items-center gap-2 rounded-lg py-2 text-black hover:bg-gray-500 md:p-2 hover:text-gray-700"
                    href="/daftarKaryawan"
                  >
                    <i
                      className={`fa-regular fa-circle  text-white ${
                        open && "hidden"
                      }`}
                    ></i>
                    <span className={`text-sm font-medium ${open && "hidden"}`}>
                      <p className="text-white">Daftar Karyawan</p>
                    </span>
                  </a>
                </div>
              )}
            </div>
            <li>
              <a
                href="diagnosa"
                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-person-dots-from-line text-white"></i>
                <span className="flex-1 text-white ml-3 whitespace-nowrap">
                  Diagnosa
                </span>
              </a>
            </li>
            <li>
              <a
                href="/penangananPertama"
                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-hands-praying text-white"></i>
                <span className="flex-1 ml-3 whitespace-nowrap text-white">
                  Penanganan Pertama
                </span>
              </a>
            </li>
            <li>
              <a
                href="/tindakan"
                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-notes-medical text-white"></i>
                <span className="flex-1 ml-3 text-white whitespace-nowrap">
                  Tindakan
                </span>
              </a>
            </li>

            <li>
              <a
                href="/daftarObat"
                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
              >
                <i className="fa-solid fa-capsules text-white"></i>
                <span className="text-white flex-1 ml-3 whitespace-nowrap">
                  Daftar Obat P3K
                </span>
              </a>
            </li>
          </ul>
          <div className="ml-2 text-white text-base mt-3">
            <Jam />
          </div>

          <div>
            <p
              onClick={logout}
              className="fixed  mt-[4rem] flex gap-2 items-center p-2 text-white rounded-lg dark:text-white hover:bg-gray-500 dark:hover:bg-gray-700"
            >
              <i className="fa-solid fa-right-from-bracket text-white"></i>
              Logout
            </p>
          </div>
        </div>
      </aside>
    </div>
  );
}
