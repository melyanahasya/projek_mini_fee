import React from "react";
import { useState } from "react";
import { useHistory, useParams  } from 'react-router-dom/cjs/react-router-dom';
import axios from "axios";
import Swal from "sweetalert2";
import { useEffect } from "react";
import Tindakan from "./Tindakan";

export default function Tangani() {
  const [username, setUsername] = useState("");
  const [statusPasien, setStatusPasien] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const history = useHistory();
  const param = useParams();
  const[show, setShow] = useState(false);
  const [penyakitId, setPenyakitId] = useState("");
  const [tanganiId, setTanganiId] = useState("");
  const [PenangananId, setPenangananId] = useState("");
  const [periksaPasien, setPeriksaPasien] = useState ({
    username:"",
    status:"",
    keluhan:"",
  })
  const[listDiagnosa, setListDiganosa] = useState([]);
  const[listPenanganan, setListPenanganan] = useState([]);
  const[listTindakan, setListTindakan] = useState([]);
  const[pages, setPages] = useState(0);
  const[tangani, setTangani] = useState("sudah ditangani")

  const getAllDiagnosa = async (page = 0) => {
    await axios
      .get(`http://localhost:2099/diagnosa/all-diagnosa?page=${page}&userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListDiganosa(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllPenanganan = async (page = 0) => {
    await axios
      .get(`http://localhost:2099/penanganan/all-penanganan?page=${page}&userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListPenanganan(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllTindakan = async (page = 0) => {
    await axios
      .get(`http://localhost:2099/tindakan/all-Tindakan?page=${pages}&userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListTindakan(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllDiagnosa(0);
    getAllPenanganan(0);
    getAllTindakan(0);
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:2099/periksa_pasien/" + param.id)
      .then((response) => {
        const tangani = response.data.data;
        setUsername(tangani.namaPasien.username);
        setStatusPasien(tangani.statusPasien);
        setKeluhan(tangani.keluhan);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  const put = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        (`http://localhost:2099/periksa_pasien/${param.id}?keluhan=${keluhan}&panyakitId=${penyakitId}&penangananId=${PenangananId}&status=${tangani}&tindakanId=${tanganiId}`), {
    });
    setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Ditambah",
        showConfirmButton: false,
        timer: 1500,
      });
    setTimeout(() => {
      history.push("/periksaPasien");
      window.location.reload();
    }, 1500);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="flex">
     

    <div className=" border h-fit ml-[3.1rem] mt-10 shadow-blue-400 shadow-lg">
        <form className="space-y-3 mt-7" onSubmit={put}>
        <div className="bg-blue-400 h-10">
            <p className="text-center pt-2 text-white">Periksa Pasien</p>
        </div>
        <div className="p-5">
      <div className="flex gap-[5rem]">
          <div>
            <label className="block mb-2 text-sm font-medium text-black dark:text-black">
              Nama Pasien
            </label>
            <p
              className="border border-gray-300 h-10 bg-gray-50 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
            >{username}</p>
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-black dark:text-black">
              Status Pasien
            </label>
            <p
              className="border border-gray-300 h-10 bg-gray-50 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
            >{statusPasien}</p>
          </div>
      </div>
      <div className="mt-5">
        <label className="block mb-2 text-sm font-medium text-black dark:text-black">
          Keluhan Pasien
        </label>
        <p
              className="border border-gray-300 h-16 bg-gray-50 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
            >{keluhan}</p>
      </div>
      <div className="flex gap-[2rem] mt-5">
        <fieldset>
          <label
            htmlFor="frm-whatever"
            className="block mb-2 text-sm font-medium text-black dark:text-black"
          >
            Penyakit Pasien
          </label>
          <div className="md:relative border w-[14rem] flex border-gray-300 text-gray-800 bg-white">
            <select
              className="appearance-none py-1 px-2 md:text-base w-[14rem]  text-sm h-10 bg-white"
              name="whatever"
              id="frm-whatever"
              onChange={(e) => setPenyakitId(e.target.value)}
            >
              <option selected>Pilih Penyakit</option>
              {listDiagnosa.map((data, index) => {
                return( <option key={index} value={data.id}>{data.namaDiagnosa}
                </option>
                )
              })}
             
            </select>
            <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
              <svg
                className="h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </fieldset>
        <fieldset>
          <label
            htmlFor="frm-whatever"
            className="block mb-2 text-sm font-medium text-black dark:text-black"
          >
            Penanganan Pertama
          </label>
          <div className="md:relative border w-[14rem] flex border-gray-300 text-gray-800 bg-white">
            <select
              className="appearance-none py-1 px-2 md:text-base w-[14rem]  text-sm h-10 bg-white"
              name="whatever"
              id="frm-whatever"
              onChange={(e) => setPenangananId(e.target.value)}
            >
              <option selected>Pilih Penanganan</option>
              {listPenanganan.map((data, index) => {
                return( <option key={index} value={data.id}>{data.namaPenanganan}
                </option>
                )
              })}
            </select>
            <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
              <svg
                className="h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </fieldset>
        <fieldset>
          <label
            htmlFor="frm-whatever"
            className="block mb-2 text-sm font-medium text-black dark:text-black"
          >
            Tindakan
          </label>
          <div className="md:relative border w-[14rem] flex border-gray-300 text-gray-800 bg-white">
            <select
              className="appearance-none py-1 px-2 md:text-base w-[14rem]  text-sm h-10 bg-white"
              name="whatever"
              id="frm-whatever"
              onChange={(e) => setTanganiId(e.target.value)}
            >
              <option selected>Pilih Tindakan</option>
              {listTindakan.map((data, index) => {
                return(
                   <option key={index} value={data.id}>{data.namaTindakan}
                </option>
                )
              })}
            </select>
            <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
              <svg
                className="h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </fieldset>
        <div>
            <label className="block mb-2 text-sm font-medium text-black dark:text-black">
              Catatan
            </label>
            <input
              placeholder=" Status Pasien"
              className="bg-white border border-gray-300 h-10 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
              onChange={(e) => setKeluhan(e.target.value)}
              value={keluhan}
            />
          </div>
        </div>
        </div>
        <button className="bg-blue-400 w-full h-10 border-r border-l border-b border-b-blue-400 border-l-blue-400 border-r-blue-400">
            <p className="text-center  text-white">Tangani</p>
        </button>
        </form>
    </div>
    </div>
  );
}
