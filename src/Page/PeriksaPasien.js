import React from "react";
import Sidebar from "../Component/Sidebar";
import { Modal } from "react-bootstrap";
import { useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import ReactPaginate from "react-paginate";
import { useEffect } from "react";

export default function PeriksaPasien() {
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);

  const handleClose = () => setShow(false);
  const handleClose1 = () => setShow1(false);
  const handleShow = () => setShow(true);
  const handleShow1 = () => setShow1(true);
  const [namaPasien, setNamaPasien] = useState("");
  const [statusPasien, setStatusPasien] = useState("");
  const [status, setStatus] = useState("belum ditangani");
  const [pasien, setPasien] = useState([]);
  const [periksa, setPeriksa] = useState([]);
  const [totalPages, setTotalPages] = useState([]);
  const [keluhan, setKeluhan] = useState("");
  const [jabatanGuru, setJabatanGuru] = useState("Guru");
  const [jabatanSiswa, setJabatanSiswa] = useState("Siswa");
  const [jabatanKaryawan, setJabatanKaryawan] = useState("Karyawan");
  const [jabatan, setJabatan] = useState();

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:2099/periksa_pasien?keluhan=${keluhan}&namaPasien=${statusPasien}&status=belum%20ditangani&statusPasien=${namaPasien}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAllNamaPasien = async (page = 0) => {
    await axios
      .get(`http://localhost:2099/periksa_pasien?page=${page}`)
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        setPeriksa(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allguru, setAllGuru] = useState([]);
  const getAllGuru = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/getAll-guru?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setAllGuru(res.data.data.totalPages);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allSiswa, setAllSiswa] = useState([]);
  const getAllSiswa = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-siswa?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setAllSiswa(res.data.data.totalPages);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allkaryawan, setAllKaryawan] = useState([]);
  const getAllKaryawan = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-karyawan?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setAllKaryawan(res.data.data.totalPages);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllPasien = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-userId?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [tanggal, setTanggal] = useState([]);
  const [filter, setFilter] = useState([]);
  const [filter_tanggal, setFilterTanggal] = useState([]);
  const [all_tanggal, setAllTanggal] = useState({
    tanggal: [],
  });

  const getAllFilter = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/periksa_pasien/filterTanggal?query=${tanggal}`
      )
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllNamaPasien(0);
    getAllGuru(0);
    getAllSiswa(0);
    getAllKaryawan(0);
    getAllPasien(0);
    getAllFilter(0);
  }, []);

  return (
    <div className="mt-20">
      <Sidebar />
      <div className="ml-[18rem] mb-11 pr-5">
        <div className="flex bg-purple-300">
          <p className="pl-6 h-8 p-2 text-xl">Filter rekap Data</p>
          <div className="ml-auto p-2">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 w-[4rem] md:w-[8rem] md:ml-2 text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={() => setShow1(true)}
            >
              Filter Tanggal
            </button>
          </div>
        </div>
        <div className="border shadow-lg h-[10rem]">
          {/* <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
              className="w-20 h-20 ml-[28rem] mt-3 dark:text-gray-600"
            >
              <path
                fill="currentColor"
                d="M256,16C123.452,16,16,123.452,16,256S123.452,496,256,496,496,388.548,496,256,388.548,16,256,16ZM403.078,403.078a207.253,207.253,0,1,1,44.589-66.125A207.332,207.332,0,0,1,403.078,403.078Z"
              ></path>
              <rect
                width="176"
                height="32"
                x="168"
                y="320"
                fill="currentColor"
              ></rect>
              <polygon
                fill="currentColor"
                points="210.63 228.042 186.588 206.671 207.958 182.63 184.042 161.37 162.671 185.412 138.63 164.042 117.37 187.958 141.412 209.329 120.042 233.37 143.958 254.63 165.329 230.588 189.37 251.958 210.63 228.042"
              ></polygon>
              <polygon
                fill="currentColor"
                points="383.958 182.63 360.042 161.37 338.671 185.412 314.63 164.042 293.37 187.958 317.412 209.329 296.042 233.37 319.958 254.63 341.329 230.588 365.37 251.958 386.63 228.042 362.588 206.671 383.958 182.63"
              ></polygon>
            </svg>
            <p className="w-[17rem] text-xl mt-3 ml-[23rem]">
              Filter terlebih dahulu sesuai tanggal yang diinginkan.
            </p>
          </div> */}

          {all_tanggal.tanggal ? (
            <div>
              <p className="text-3xl text-medium text-bold ml-[24rem] mt-5">
                Rekap Data Pasien
              </p>
              <div className="flex gap-3 text-lg mt-5 ml-[24.8rem]">
                <p>{tanggal}</p>
              </div>
              <button
                data-modal-target="authentication-modal"
                data-modal-toggle="authentication-modal"
                className="md:text-base mt-2 md:mt-3 w-[4rem] md:w-[15rem] md:ml-[24.2rem] text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                type="submit"
                //   onClick={handleShow}
              >
                Download Rekap Data Pasien
              </button>
            </div>
          ) : (
            <></>
          )}
        </div>
      </div>
      <div className="ml-[18rem] pr-5">
        <div className="flex bg-purple-300">
          <p className="pl-6 h-10 p-2 text-xl">Daftar Pasien</p>
          <div className="ml-auto p-2">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 w-[4rem] md:w-[5.5rem] md:ml-2 text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={handleShow}
            >
              Tambah
            </button>
          </div>
        </div>
        <table className="min-w-full border shadow-lg text-center">
          <thead className="border-b">
            <tr>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-9 md:py-4 py-2 border-r"
              >
                No
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-28 md:py-4 py-2 border-r"
              >
                Nama Pasien
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Status Pasien
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Jabatan
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Tanggal
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Keterangan
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Status
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            {periksa.map((pasien, idx) => {
              return (
                <tr className="border-b">
                  <td className="px-3 md:py-3 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                    {idx + 1}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.namaPasien.username}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.namaPasien.status}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.namaPasien.status}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.tanggal}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.keluhan}
                  </td>

                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-3 py-2 whitespace-nowrap border-r">
                    {pasien.status}
                  </td>
                  <td className="md:text-sm text-xs ml-[2rem] text-gray-900 font-light px-3 md:py-3 py-2  whitespace-nowrap flex gap-3 border-r">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25"
                      height="25"
                      fill="currentColor"
                      className="bi bi-cloud-download"
                      viewBox="0 0 16 16"
                    >
                      <path d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z"></path>
                      <path d="M7.646 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V5.5a.5.5 0 0 0-1 0v8.793l-2.146-2.147a.5.5 0 0 0-.708.708l3 3z"></path>
                    </svg>
                    <a
                      href="statusPeriksa"
                      className="border text-white rounded p-1 font-medium bg-red-500 w-16"
                    >
                      Tangani
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>{" "}
      </div>{" "}
      {/* Pagination */}
      <ReactPaginate
        className="flex gap-2 mt-3 justify-center"
        previousLabel={"<"}
        nextLabel={">"}
        breakLabel={"..."}
        pageCount={totalPages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={(e) => getAllPasien(e.selected)}
        pageClassName={
          "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-violet-300 border-violet-200 bg-violet-200 rounded"
        }
        pageLinkClassName={"page-link"}
        previousClassName={
          "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 border-gray-200"
        }
        previousLinkClassName={"page-link"}
        nextClassName={
          "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 border-gray-200 md:h-7 h-6 text-xs"
        }
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />
      {/* modal add */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4  md:text-xl text-base font-medium text-black dark:text-black">
                Tambah Periksa Pasien
              </h3>
              <form className="space-y-3" onSubmit={add}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Status Pasien
                  </label>

                  <select
                    className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
                    name="whatever"
                    id="frm-whatever"
                    onChange={(e) => setNamaPasien(e.target.value)}
                  >
                    <option>Pilih Status Pasien</option>
                    <option value="Guru">Guru</option>
                    <option value="Siswa">Siswa</option>
                    <option value="Karyawan">Karyawan</option>
                  </select>
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Pasien
                  </label>

                  {namaPasien == "Guru" ? (
                    <>
                      <select
                        className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
                        name="whatever"
                        id="frm-whatever"
                        onChange={(e) => setStatusPasien(e.target.value)}
                      >
                        <option value="">Pilih Nama Pasien</option>
                        {allguru.map((down, idx) => {
                          return (
                            <option key={idx} value={down.id}>
                              <p>{down.username}</p>
                            </option>
                          );
                        })}
                      </select>
                    </>
                  ) : namaPasien == "Siswa" ? (
                    <>
                      {" "}
                      <div>
                        <select
                          className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
                          name="whatever"
                          id="frm-whatever"
                          onChange={(e) => setStatusPasien(e.target.value)}
                        >
                          <option value="">Pilih Nama Pasien</option>
                          {allSiswa.map((down, idx) => {
                            return (
                              <option key={idx} value={down.id}>
                                <p>{down.username}</p>
                              </option>
                            );
                          })}
                        </select>
                      </div>{" "}
                    </>
                  ) : namaPasien == "Karyawan" ? (
                    <>
                      {" "}
                      <div>
                        <select
                          className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
                          name="whatever"
                          id="frm-whatever"
                          onChange={(e) => setStatusPasien(e.target.value)}
                        >
                          <option value="">Pilih Nama Pasien</option>
                          {allkaryawan.map((down, idx) => {
                            return (
                              <option key={idx} value={down.id}>
                                <p>{down.username}</p>
                              </option>
                            );
                          })}
                        </select>
                      </div>{" "}
                    </>
                  ) : (
                    <>
                      <div>
                        <select
                          className="ml-10 mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg md:float-right focus:ring-blue-500 focus:border-blue-500 block w-[100%] md:p-1.5 p-1 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-700 dark:focus:ring-blue-500 dark:focus:border-blue-500 "
                          name="whatever"
                          id="frm-whatever"
                          onChange={(e) => setStatusPasien(e.target.value)}
                        >
                          <option>Pilih Nama Pasien</option>
                        </select>
                      </div>
                    </>
                  )}
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Keluhan Pasien
                  </label>
                  <input
                    placeholder="Keluhan Pasien"
                    onChange={(e) => setKeluhan(e.target.value)}
                    value={keluhan}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div className="flex gap-3 pt-3 ml-[14rem]">
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </button>
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      {/* modal filter */}
      <Modal
        show={show1}
        onHide={!show1}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={() => setShow1(false)}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4  md:text-xl text-base font-medium text-black dark:text-black">
                Filter Tanggal
              </h3>
              <form
                className="space-y-3"
                //  onSubmit={add}
              >
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tanggal
                  </label>
                  <input
                    placeholder="Nama Pasien"
                    value={tanggal}
                    onChange={(e) => setTanggal(e.target.value)}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                    type="date"
                  />
                </div>

                <div className="flex gap-3 pt-3 ml-[14rem]">
                  <button
                    onClick={(e) => {
                      e.preventDefault();
                      setShow1(false);
                      getAllFilter(0);
                    }}
                    type="submit"
                    className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </button>
                  <button
                    onClick={handleClose1}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
