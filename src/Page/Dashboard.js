import React, { useEffect, useState } from "react";
import Sidebar from "../Component/Sidebar";
import axios from "axios";
import ReactPaginate from "react-paginate";

export default function Dashboard() {
  const [namaGuru, setNamaGuru] = useState("");
  const [tempatTanggalLahir, setTempatTanggalLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [listDaftarGuru, setListDaftarGuru] = useState([]);
  const [pages, setPages] = useState(0);
  const [kelas, setKelas] = useState("");
  const [namaSiswa, setNamaSiswa] = useState("");
  const [listDataSiswa, setListDataSiswa] = useState([]);
  const [namaKaryawan, setNamaKaryawan] = useState("");
  const [listDataKaryawan, setListDataKaryawan] = useState([]);
  const [list, setList] = useState([]);

  const getAllGuru = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/getAll-guru?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDaftarGuru(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-userId?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setPages(res.data.data.totalPages);
        setList(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllKaryawan = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-karyawan?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDataKaryawan(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllSiswa = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-siswa?&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDataSiswa(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllGuru(0);
    getAllSiswa(0);
    getAllKaryawan(0);
    getAll(0);
  }, []);
  return (
    <div className=" mt-14">
      <div>
        <Sidebar />
      </div>

      <div className="pl-[15.5rem]">
        <div className="md:flex block gap-5 ml-11 text-center">
          <div className=" shadow-lg border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-28 border-t-purple-300 border-t-4">
            <p className="text-lg text-bold font-medium ">Daftar Pasien Guru</p>
            <p className="text-lg text-violet-300  font-bold md:text-2xl pt-2">
              <i className="fa-solid fa-wheelchair text-4xl"></i>{" "}
              {listDaftarGuru.length} Guru
            </p>
          </div>
          <div className="shadow-lg border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-28 border-t-purple-300 border-t-4">
            <p className="text-lg text-bold font-medium ">
              Daftar Pasien Siswa
            </p>
            <p className="text-lg text-violet-300  font-bold md:text-2xl pt-2">
              <i className="fa-solid fa-wheelchair text-4xl"></i>{" "}
              {listDataSiswa.length} Siswa
            </p>
          </div>
          <div className="shadow-lg border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-28 border-purple-300 border-t-4">
            <p className="text-lg text-bold font-medium ">
              Daftar Pasien Karyawan
            </p>
            <p className="text-lg text-violet-300 font-bold md:text-2xl pt-2">
              <i className="fa-solid fa-wheelchair text-4xl"></i>{" "}
              {listDataKaryawan.length} Karyawan
            </p>
          </div>
        </div>

        <div className="pl-12 pr-11">
          <div className="border shadow-lg w-fit md:w-full mt-8">
            <p className="text-lg h-10 p-1 text-center text-bold font-medium bg-purple-300">
              Riwayat Pasien
            </p>
            <table className="min-w-full border text-center">
              <thead className="border-b">
                <tr>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium text-gray-900 md:w-9 md:py-4 py-2 border-r"
                  >
                    No
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium text-gray-900 w-96 md:py-4 py-2 border-r"
                  >
                    Nama Pasien
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium text-gray-900 px-6 md:py-4 py-2 border-r"
                  >
                    Status Pasien
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
                  >
                    Tanggal / Jam pemeriksaan
                  </th>
                </tr>
              </thead>
              <tbody>
                {list.map((dashboard, index) => {
                  return (
                    <tr className="border-b">
                      <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                        {index + 1}
                      </td>
                      <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap border-r">
                        {dashboard.username}
                      </td>
                      <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                        {dashboard.status}
                      </td>
                      <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                        {dashboard.tanggalLahir}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>{" "}
          {/* Pagination */}
          <ReactPaginate
            className="flex gap-2 mt-3 justify-center"
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={"..."}
            pageCount={pages}
            marginPagesDisplayed={2}
            pageRangeDisplayed={3}
            onPageChange={(e) => getAll(e.selected)}
            pageClassName={
              "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-violet-300 border-violet-200 bg-violet-200 rounded"
            }
            pageLinkClassName={"page-link"}
            previousClassName={
              "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 border-gray-200"
            }
            previousLinkClassName={"page-link"}
            nextClassName={
              "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 border-gray-200 md:h-7 h-6 text-xs"
            }
            nextLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
          ></ReactPaginate>
        </div>
      </div>
    </div>
  );
}
