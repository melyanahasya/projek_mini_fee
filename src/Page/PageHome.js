import React from "react";
import Sidebar from "../Component/Sidebar";

export default function PageHome() {
  return (
    <div className="">
      <Sidebar />

      {/* <div className='pl-[10rem] bg-[url("https://i.pinimg.com/564x/bf/65/3c/bf653cf17c10d9367d7b8aa60d3fcbed.jpg")] bg-no-repeat bg-cover w-full sepia-0'>
        <div className='border-blue-200 bg-[url("https://i.pinimg.com/564x/bf/65/3c/bf653cf17c10d9367d7b8aa60d3fcbed.jpg")] bg-no-repeat bg-cover w-[70rem] md:w-[59rem] md:absolute md:ml-[10rem] md:mt-[5rem] mt-[3rem] shadow-red-200 shadow-2xl mb-1 border h-[28rem] md:rounded-xl rounded-l-xl'>
          <div className=" md:mt-[13rem] md:text-left ml-[10%] mt-[2rem] md:ml-[2rem]">
            <img
              className="md:hidden static"
              src="https://www.zimozi.co/wp-content/uploads/2022/09/final-dl.beatsnoop.com-ejZrJoVFBT-removebg-preview-edited.png"
              alt=""
            />
            <p className="md:text-2xl text-gray-700 text-xl font-bold ">
              SELAMAT DATANG MELYANA
            </p>
            <p className="text-sm ml-[5rem]">Sistem Aplikasi UKS</p>
          </div>
        </div>
      </div> */}

      <div className="pl-[10rem] w-full h-full bg-no-repeat bg-cover bg-[url('https://www.katolikana.com/wp-content/uploads/2018/05/bg-login-katolikana.jpg')] md:absolute">
        <p className="text-center mt-[18rem] text-black text-3xl font-serif  font-bold">
          SELAMAT DATANG 
        </p>
        <p className="text-center text-black text-xl">
          Di Sistem Aplikasi UKS
        </p>
      </div>
    </div>
  );
}
