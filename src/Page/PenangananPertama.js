import React, { useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import { Modal } from "react-bootstrap";
import { useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import ReactPaginate from "react-paginate";

export default function PenangananPertama() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [list, setList] = useState([]);
  const [totalPages, setTotalPages] = useState([]);

  const [nama_penanganan, setNamaPenanganan] = useState("");
  const [penanganan_pertama, setPenangananPertama] = useState("");

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:2099/penanganan_pertama?namaPenangananPertama=${nama_penanganan}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async (page = 0) => {
    await axios
      .get(`http://localhost:2099/penanganan_pertama/all?page=${page}&search=${penanganan_pertama}`)
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        setList(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2099/penanganan_pertama/` + id);
        Swal.fire({
          icon: "success",
          title: "Sukses Menghapus",
          showConfirmButton: false,
        });
      }
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    });
  };
  return (
    <div className="mt-20 pr-5">
      <Sidebar />

      <div className="pl-[18rem]">
        <div className="flex bg-purple-300">
          <p className="pl-6 h-10 p-2 text-xl">Penanganan Pertama</p>
          <div className="ml-auto p-2">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 w-[4rem] md:w-[5.5rem] md:ml-2 text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={handleShow}
            >
              Tambah
            </button>
          </div>
        </div>
        <table className="min-w-full border shadow-lg text-center">
          <thead className="border-b">
            <tr>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-7 md:py-4 py-2 border-r"
              >
                No
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-[35rem] md:py-4 py-2 border-r"
              >
                Nama Penanganan
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            {list.map((penanganan, index) => {
              return (
                <tr className="border-b">
                  <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                    {index + 1}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                    {penanganan.penangananPertama}
                  </td>
                  <td className="md:text-sm text-xs ml-[7.5rem] text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap flex gap-3 border-r">
                  <a href={"/editPenanganan/" + penanganan.id}>
                      <i className="fas fa-edit text-green-500 text-lg"></i>
                    </a>
                    <div>
                      <button onClick={() => deleteUser(penanganan.id)}>
                        <i className="fas fa-trash-alt text-red-500 text-lg"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      {/* Pagination */}
      <ReactPaginate
        className="flex gap-2 mt-3 justify-center"
        previousLabel={"<"}
        nextLabel={">"}
        breakLabel={"..."}
        pageCount={totalPages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={(e) => getAll(e.selected)}
        pageClassName={
          "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-violet-300 border-violet-200 bg-violet-200 rounded"
        }
        pageLinkClassName={"page-link"}
        previousClassName={
          "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 border-gray-200"
        }
        previousLinkClassName={"page-link"}
        nextClassName={
          "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 border-gray-200 md:h-7 h-6 text-xs"
        }
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />

      {/* modal add */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4  md:text-xl text-base font-medium text-black dark:text-black">
                Tambah Penanganan Pertama
              </h3>
              <form className="space-y-3" onSubmit={add}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Penanganan
                  </label>
                  <input
                    placeholder="Nama Penanganan"
                    onChange={(e) => setNamaPenanganan(e.target.value)}
                    value={nama_penanganan}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div className="flex gap-3 pt-3 ml-[14rem]">
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </button>
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
