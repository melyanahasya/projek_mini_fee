import React, { useState } from "react";
import Sidebar from "../Component/Sidebar";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import axios from "axios";
import ReactPaginate from "react-paginate";
import { useEffect } from "react";

export default function DaftarSiswa() {
  const [show, setShow] = useState(false);
  const [impor, setImpor] = useState(false);
  const [download, setDownload] = useState(false);

  const handleClose = () => setShow(false);
  const handleClose1 = () => setImpor(false);
  const handleShow = () => setShow(true);
  const handleShow2 = () => setImpor(true);
  const handleShow3 = () => setDownload(true);
  const [list, setList] = useState([]);
  const [totalPages, setTotalPages] = useState([]);

  const [alamat, setAlamat] = useState("");
  const [username, setUsername] = useState("");
  const [kelas, setKelas] = useState("");
  const [tempat, setTempat] = useState("");
  const [jabatan, setJabatan] = useState("");
  const [tanggal_lahir, setTanggalLahir] = useState("");

  const [daftar_siswa, setDaftarSiswa] = useState("");

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:2099/pasien/post-siswa?alamat=${alamat}&jabatan=${jabatan}&kelas=${kelas}&tanggalLahir=${tanggal_lahir}&tempat=${tempat}&userId=${localStorage.getItem("userId")}&username=${username}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };


  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2099/pasien/all-siswa?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        setList(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2099/pasien/` + id);
        Swal.fire({
          icon: "success",
          title: "Sukses Menghapus",
          showConfirmButton: false,
        });
      }
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    });
  };

  const handleShowExcel = () => setShowExcel(true);
  const [showExcel, setShowExcel] = useState(false);
  const handleCloseExcel = () => setShowExcel(false);
  const [loading, setLoading] = useState(false);
  const downloadFormat = async () => {
    await Swal.fire({
      title: "Yakin Ingin Mendownload?",
      text: "File berisi semua data siswa",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2099/api/siswaExcell/download/template_siswa
          `,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setLoading(true);
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "format-daftar-siswa.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
            handleCloseExcel();
            setLoading(false);
          }, 2000);
        });
      }
    });
  };
  return (
    <div className="mt-20  pr-5">
      <Sidebar />

      <div className="pl-[18rem]">
        <div className="flex bg-purple-300">
          <p className="pl-6 h-10 p-2 text-xl"> Daftar Siswa</p>
          <div className="ml-auto p-2">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 w-[4rem] md:w-[5.5rem] md:ml-2 text-white bg-gray-600 hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={handleShow}
            >
              Tambah
            </button>
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 md:w-[7rem] md:ml-2 text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={handleShow2}
            >
              Import Data
            </button>
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              className="md:text-base mt-2 md:mt-0 md:w-[8.5rem] md:ml-2 text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded text-xs md:h-8 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
              type="submit"
              onClick={downloadFormat}
            >
              Download Data
            </button>
          </div>
        </div>
        <table className="min-w-full border shadow-lg text-center">
          <thead className="border-b">
            <tr>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-9 md:py-4 py-2 border-r"
              >
                No
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Nama Siswa
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Kelas
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Tempat Tanggal Lahir
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Alamat
              </th>
              <th
                scope="col"
                className="md:text-sm text-xs font-medium text-gray-900 md:w-60 w-20 md:py-4 py-2 border-r"
              >
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            {list.map((siswa, index) => {
              return (
                <tr className="border-b">
                  <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                    {index + 1}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                    {siswa.username}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                    {siswa.kelas}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                    {siswa.tempat} {siswa.ttl}
                  </td>
                  <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap border-r">
                    {siswa.alamat}
                  </td>
                  <td className="md:text-sm text-xs ml-[4.5rem] text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap flex gap-3 border-r">
                  <a href={"/editdaftarSiswa/" + siswa.id}>
                      <i className="fas fa-edit text-green-500 text-lg"></i>
                    </a>
                    <div>
                      <button onClick={() => deleteUser(siswa.id)}>
                        <i className="fas fa-trash-alt text-red-500 text-lg"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      {/* Pagination */}
      <ReactPaginate
        className="flex gap-2 mt-3 justify-center"
        previousLabel={"<"}
        nextLabel={">"}
        breakLabel={"..."}
        pageCount={totalPages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={(e) => getAll(e.selected)}
        pageClassName={
          "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-violet-300 border-violet-200 bg-violet-200 rounded"
        }
        pageLinkClassName={"page-link"}
        previousClassName={
          "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 border-gray-200"
        }
        previousLinkClassName={"page-link"}
        nextClassName={
          "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 border-gray-200 md:h-7 h-6 text-xs"
        }
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />

      {/* modal add */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambah Daftar Siswa
              </h3>
              <form className="space-y-3" onSubmit={add}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Siswa
                  </label>
                  <input
                    placeholder="Nama Siswa"
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Kelas
                  </label>
                  <input
                    placeholder="Kelas"
                    onChange={(e) => setKelas(e.target.value)}
                    value={kelas}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tempat Lahir
                  </label>
                  <input
                    placeholder="Tempat Lahir"
                    onChange={(e) => setTempat(e.target.value)}
                    value={tempat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tanggal Lahir
                  </label>
                  <input
                    placeholder=" Tanggal Lahir"
                    onChange={(e) => setTanggalLahir(e.target.value)}
                    value={tanggal_lahir}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                    type="date"
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Alamat
                  </label>
                  <input
                    placeholder="Alamat"
                    onChange={(e) => setAlamat(e.target.value)}
                    value={alamat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div className="flex gap-3 pt-3 ml-[14rem]">
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </button>
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>

      {/* modal import excell */}
      <Modal
        show={impor}
        onHide={!impor}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={() => setImpor(false)}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Import Guru Dari File Excell
              </h3>
              <form
                className="space-y-3"
                // onSubmit={postPena}
              >
                <div className="h-[7rem] bg-gray-50 px-5 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black">
                  <p className="px-1 mt-1">
                    Download file dibawah untuk menginput data guru anda,
                  </p>
                  <p className="px-5 text-bold text-medium">
                    ( *column tanggal lahir diubah menjadi short date )
                  </p>

                  <button className="border text-white shadow shadow-violet-400 ml-[6.5rem] mt-3 bg-violet-400 rounded px-3 py-1 text-center">
                    Download Format File
                  </button>
                </div>

                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Drop File.xlsx
                  </label>
                  <input
                    type="file"
                    // onChange={(e) => setTtl(e.target.value)}
                    // value={ttl}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <div className="flex gap-3 pt-3 ml-[14rem]">
                  <button
                    // onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                  <button
                    onClick={handleClose1}
                    type="submit"
                    className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded text-sm px-2 py-1 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>

      {/* modal download excell */}
      {/* <Modal
        show={download}
        onHide={!download}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={() => setDownload(false)}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
              Yakin Ingin Mendownload
              </h3>
              <form
                className="space-y-3"
                onSubmit={downloadFormat}
              >
                <div className="h-[7rem] bg-gray-50 px-5 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black">
                  <p className="text-center mt-3">File Berisi Semua Data Siswa</p>

                  <button className="border text-white shadow shadow-violet-400 ml-[6.5rem] mt-3 bg-violet-400 rounded px-3 py-1 text-center">
                    Download Format File
                  </button>
                </div>

              
              </form>
            </div>
          </div>
        </div>
      </Modal> */}
    </div>
  );
}
