import React, { useEffect } from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import { Form, InputGroup, Modal } from "react-bootstrap";
import bcrypt from "bcryptjs";
import Sidebar from "../Component/Sidebar";

export default function Profile() {
  const [username, setUserName] = useState("");
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleShow1 = () => setShow(true);
  const [password, setPassword] = useState("");
  const [alamat, setAlamat] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [foto, setFoto] = useState(null);
  const [passwordType, setPasswordType] = useState("password");
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");

  const [passLama, setPassLama] = useState("");
  const [conPassLama, setConPassLama] = useState("");
  const [passBaru, setPassBaru] = useState("");
  const [conPassBaru, setConPassBaru] = useState("");

  const [profile, setProfile] = useState({
    username: "",
    email: "",
    alamat: "",
    tanggalLahir: "",
    password: "",
    foto: null,
  });

  const getAll = async () => {
    await axios
      .get("http://localhost:2099/register/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:2099/register/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        setUserName(profil.username);
        setEmail(profil.email);
        setAlamat(profil.alamat);
        setTanggalLahir(profil.tanggalLahir);
        setFoto(profile.foto);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const Edit = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        `http://localhost:2099/register/` + localStorage.getItem("userId"),
        {
          username: username,
          email: email,
          alamat: alamat,
          tanggalLahir: tanggalLahir,
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/profile");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const putFoto = async (e) => {
    e.preventDefault();
    e.persist();

    const data = new FormData();
    data.append("file", foto);

    console.log(foto);
    try {
      await axios.put(
        `http://localhost:2099/register/foto/${localStorage.getItem("userId")}`,
        data
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
        timer: 5000,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (err) {
      console.log(err);
    }
  };

  const conPassword = async () => {
    await axios
      .get("http://localhost:2099/register/" + localStorage.getItem("userId"))
      .then((res) => {
        setPassLama(res.data.data.password);
      });
  };
  useEffect(() => {
    conPassword();
  }, []);

  const ubahPass = (e) => {
    e.preventDefault();
    bcrypt.compare(conPassLama, passLama, function (err, isMatch) {
      if (err) {
        throw err;
      } else if (!isMatch) {
        Swal.fire({
          icon: "error",
          title: "Password Tidak sama dengan yang sebelumnya",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        if (passBaru === conPassLama) {
          Swal.fire({
            icon: "error",
            title: "Password tidak boleh sama dengan sebelumnya",
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          if (passBaru === conPassBaru) {
            axios
              .put(
                "http://localhost:2099/register/password/" +
                  localStorage.getItem("userId"),
                {
                  password: passBaru,
                }
              )
              .then(() => {
                Swal.fire({
                  icon: "success",
                  title: " Berhasil Mengubah Password",
                  showConfirmButton: false,
                  timer: 1500,
                });
                setTimeout(() => {
                  window.location.reload();
                }, 1500);
              })
              .catch((err) => {
                Swal.fire({
                  icon: "error",
                  title: "Password minimal 8-20 karater, angka, huruf kecil",
                  showConfirmButton: false,
                  timer: 1500,
                });
                console.log(err);
              });
          } else {
            Swal.fire({
              icon: "error",
              title: "Password Tidak sama",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        }
      }
    });
  };

  const togglePassword = () => {
    console.log(passwordType);
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };

  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text");
      return;
    }
    setPasswordType1("password");
  };

  const togglePassword2 = () => {
    if (passwordType2 === "password") {
      setPasswordType2("text");
      return;
    }
    setPasswordType2("password");
  };

  return (
    <div>
      <Sidebar />

      <div className="mt-8 ml-[16rem]">
        <main>
          <div className=" h-72 block h-500-px">
            <div className="absolute top-0 w-[68rem] h-72 bg-center bg-cover bg-[url('https://cdn.wallpapersafari.com/21/14/TPLZrB.jpg')]">
              <span
                id="blackOverlay"
                className="w-full h-full absolute opacity-50 bg-black"
              ></span>
            </div>
            <div className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px">
              <svg
                className="absolute bottom-0 overflow-hidden"
                preserveAspectRatio="none"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="text-blueGray-200 fill-current"
                  points="2560 0 2560 100 0 100"
                ></polygon>
              </svg>
            </div>
          </div>
          <form
            // onSubmit={putProfile}
            className="relative py-16 md:px-16 bg-blueGray-200"
          >
            <div className="container mx-auto px-4 ">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
                <div className="px-6">
                  <div className="flex flex-wrap justify-center">
                    <div className="md:w-full w-12  px-4 lg:order-2 flex justify-center">
                      <div>
                        <button>
                          {profile.foto ? (
                            <img
                              src={profile.foto}
                              alt=""
                              className="w-[1rem] border-gray-300 -ml-[29.9rem] -mt-11 border-4 max-w-[12rem] min-w-[12rem] z-10  rounded-full"
                            />
                          ) : (
                            <img
                              src="https://i.pinimg.com/736x/7b/3a/37/7b3a37aaa3c89d4a45a70d40ece74271.jpg"
                              alt=""
                              className="w-[1rem] border-gray-300 -ml-[29.9rem] -mt-11 border-4 max-w-[12rem] min-w-[12rem]  z-10  rounded-full"
                            />
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="text-center -mt-[7rem]">
                    <h3 className="md:text-4xl text-2xl font-semibold leading-normal mb-2 text-blueGray-700 ">
                      {/* Melyana */}
                      {profile.username}
                    </h3>
                    <div className="md:text-base text-xs leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                      <i className="fas fa-map-marker-alt mr-2 md:text-base text-blueGray-400"></i>
                      Alamat : {profile.alamat}
                    </div>
                    <div className="mb-2 text-blueGray-600 mt-10 md:text-base text-sm">
                      <i className="fas fa-envelope"></i> Email :{" "}
                      {profile.email}
                    </div>
                    <div className="mb-2 text-blueGray-600  md:text-base text-sm">
                      <i className="fas fa-user"></i> Username :{" "}
                      {profile.username}
                    </div>
                    <div className="mb-2 text-blueGray-600  md:text-base text-sm">
                      <i className="fa-solid fa-calendar-days"></i> Tanggal
                      Lahir : {profile.tanggalLahir}
                    </div>
                  </div>
                  <div className="mt-4 py-10 border-t border-blueGray-200 text-center">
                    <div className="flex flex-wrap justify-center">
                      <div className="w-full lg:w-9/12 px-4">
                        <p className="mb-4 md:text-lg text-base leading-relaxed text-blueGray-700">
                          Kelola informasi profil anda untuk mengontrol,
                          melindungi, dan mengamankan akun
                        </p>
                        <div className="py-6 px-3 md:mt-4 sm:mt-0">
                          <a
                            className="bg-violet-300 active:bg-violet-400 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1 ease-linear transition-all duration-150"
                            href="/editProfil"
                          >
                            Edit
                          </a>

                          <button
                            className=" bg-violet-300 active:bg-violet-400 text-white uppercase  hover:shadow-md shadow text-xs px-4 py-2 font-bold rounded"
                            type="button"
                            onClick={setShow1}
                          >
                            Edit Password
                          </button>

                          <button
                            className="ml-2 bg-violet-300 active:bg-violet-400 text-white uppercase  hover:shadow-md shadow text-xs px-4 py-2 font-bold rounded"
                            type="button"
                            onClick={handleShow}
                          >
                            Edit fOTO
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          {/* edit profile */}
          <Modal
            show={show}
            onHide={handleClose}
            id="authentication-modal"
            tabIndex="-1"
            aria-hidden="true"
            className="md:ml-[30%] new ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
          >
            <div className="relative w-full h-full max-w-md md:h-auto">
              <div className="relative bg-white rounded-lg shadow dark:bg-white">
                <button
                  type="button"
                  className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                  data-modal-hide="authentication-modal"
                  onClick={handleClose}
                >
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
                <div className="px-6 py-6 lg:px-8">
                  <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                    Edit Foto
                  </h3>
                  <form className="space-y-3" onSubmit={putFoto}>
                    <div>
                      <input
                        onChange={(e) => setFoto(e.target.files[0])}
                        type="file"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                      />
                    </div>
                    <button
                      type="submit"
                      className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      Ubah
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </Modal>

          {/* <Modal show={show} onHide={handleClose}>
            <Modal.Header
              closeButton
              className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')] bg-no-repeat bg-cover"
            >
              <Modal.Title>Edit </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={Edit}>
                <div className="mb-3">
                  <Form.Label>
                    <strong>Nama</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      placeholder="nama..."
                      value={username}
                      onChange={(e) => setUserName(e.target.value)}
                      required
                    />
                  </InputGroup>
                </div>

                <div className="mb-3">
                  <Form.Label>
                    <strong>Alamat</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      placeholder="alamat..."
                      value={alamat}
                      onChange={(e) => setAlamat(e.target.value)}
                      required
                    />
                  </InputGroup>
                </div>

                <div className="mb-3">
                  <Form.Label>
                    <strong>Gambar</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      type="file"
                      onChange={(e) => setFoto(e.target.files[0])}
                      required
                    />
                  </InputGroup>
                </div>

                <div className="mb-3">
                  <Form.Label>
                    <strong>Tanggal Lahir</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      placeholder="No telepon..."
                      type="date"
                      value={tanggalLahir}
                      onChange={(e) => setTanggalLahir(e.target.value)}
                      required
                    />
                  </InputGroup>
                </div>
                <div className="mb-3">
                  <Form.Label>
                    <strong>Email</strong>
                  </Form.Label>
                  <InputGroup className="d-flex gap-3">
                    <Form.Control
                      placeholder="email..."
                      type="text"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </InputGroup>
                </div>
                <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="password..."
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </InputGroup>
            </div>

                <button className="mx-1 button-btl btn" onClick={handleClose}>
                  Close
                </button>
                <button
                  type="submit"
                  className="mx-1 button-btl btn"
                  onClick={handleClose}
                >
                  save
                </button>
              </Form>
            </Modal.Body>
          </Modal> */}

          {/* Modal Password */}
          <Modal
            show={show1}
            onHide={!show1}
            id="authentication-modal"
            tabIndex="-1"
            aria-hidden="true"
            className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden p-4 md:inset-0 h-modal md:h-full focus:outline-none focus:ring-0"
          >
            <div className="relative w-full h-full max-w-md md:h-auto focus:outline-none focus:ring-0">
              <div className="relative bg-white rounded-lg shadow">
                <button
                  type="button"
                  className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:text-white"
                  data-modal-hide="authentication-modal"
                  onClick={() => setShow1(false)}
                >
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
                <div className="px-6 py-6 lg:px-8">
                  <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
                    Edit Password
                  </h3>
                  <form onSubmit={ubahPass} className="space-y-3">
                    <div>
                      <label htmlFor="password">Password Lama</label>
                      <div className="relative border-none focus:outline-none focus:ring-0">
                        <input
                          required
                          placeholder="Masukan password lama anda"
                          type={passwordType2}
                          className="md:h-5 mt-2 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                          onChange={(e) => setConPassLama(e.target.value)}
                        />

                        <span
                          onClick={togglePassword2}
                          className="absolute inset-y-0 right-0 grid place-content-center px-4 focus:outline-none focus:ring-0"
                        >
                          {passwordType2 === "password" ? (
                            <>
                              <i className="fa-solid fa-eye-slash mt-1"></i>
                            </>
                          ) : (
                            <>
                              <i className="fa-solid fa-eye mt-1"></i>
                            </>
                          )}
                        </span>
                      </div>
                    </div>
                    <div>
                      <label htmlFor="password">Password Baru</label>
                      <div className="relative">
                        <input
                          required
                          placeholder="Masukan password baru"
                          type={passwordType}
                          className="md:h-5 mt-2 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                          onChange={(e) => setPassBaru(e.target.value)}
                        />

                        <span
                          onClick={togglePassword}
                          className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                        >
                          {passwordType === "password" ? (
                            <>
                              <i className="fa-solid fa-eye-slash mt-1"></i>
                            </>
                          ) : (
                            <>
                              <i className="fa-solid fa-eye mt-1"></i>
                            </>
                          )}
                        </span>
                      </div>
                    </div>
                    <div>
                      <label htmlFor="password">Konfirmasi Password</label>
                      <div className="relative">
                        <input
                          required
                          placeholder="Konfirmasi Password Baru"
                          type={passwordType1}
                          className="md:h-5 w-full mt-2 rounded-lg border p-4 pr-12 text-sm shadow-sm  focus:outline-none focus:ring-0"
                          onChange={(e) => setConPassBaru(e.target.value)}
                        />

                        <span
                          onClick={togglePassword1}
                          className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                        >
                          {passwordType1 === "password" ? (
                            <>
                              <i className="fa-solid fa-eye-slash mt-1"></i>
                            </>
                          ) : (
                            <>
                              <i className="fa-solid fa-eye mt-1"></i>
                            </>
                          )}
                        </span>
                      </div>
                    </div>
                    <button
                      type="submit"
                      className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      Edit
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </Modal>
        </main>
      </div>
    </div>
  );
}
