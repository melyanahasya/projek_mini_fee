import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Dashboard from './Page/Dashboard';
import Diagnosa from './Page/Diagnosa';
import PenangananPertama from './Page/PenangananPertama';
import Tindakan from './Page/Tindakan';
import DaftarObat from './Page/DaftarObat';
import DaftarGuru from './Page/DaftarGuru';
import DaftarSiswa from './Page/DaftarSiswa';
import DaftarKaryawan from './Page/DaftarKaryawan';
import PeriksaPasien from './Page/PeriksaPasien';
import Login from './Page1/Login';
import Register from './Page1/Resgister';
import PageHome from './Page/PageHome';
import EditDaftarObat from './Edit/EditDaftarObat';
import EditTindakan from './Edit/EditTindakan';
import EditPenanganan from './Edit/EditPenanganan';
import EditDiagnosa from './Edit/EditDiagnosa';
import EditDaftarKaryawan from './Edit/EditDaftarKaryawan';
import EditDaftarSiswa from './Edit/EditDaftarSiswa';
import EditDaftarGuru from './Edit/EditDaftarGuru';
import StatusPeriksa from './Edit/StatusPeriksa';
import Profile from './Page/Profile';
import EditProfil from './Edit/EditProfil';
import Tangani from './Page/Tangani';

function App() {
  return (
    <div>
      <BrowserRouter>
    
      <Switch>
        <Route path="/" component={Login} exact/>
        <Route path="/register" component={Register} exact/>
        <Route path="/home" component={PageHome} exact/>
        <Route path="/dashboard" component={Dashboard} exact/>
        <Route path="/diagnosa" component={Diagnosa} exact/>
        <Route path="/penangananPertama" component={PenangananPertama} exact/>
        <Route path="/tindakan" component={Tindakan} exact/>
        <Route path="/daftarObat" component={DaftarObat} exact/>
        <Route path="/daftarGuru" component={DaftarGuru} exact/>
        <Route path="/daftarSiswa" component={DaftarSiswa} exact/>
        <Route path="/daftarKaryawan" component={DaftarKaryawan} exact/>
        <Route path="/periksaPasien" component={PeriksaPasien} exact/>
        <Route path="/editDaftarObat/:id" component={EditDaftarObat} exact/>
        <Route path="/editTindakan/:id" component={EditTindakan} exact/>
        <Route path="/editPenanganan/:id" component={EditPenanganan} exact/>
        <Route path="/editDiagnosa/:id" component={EditDiagnosa} exact/>
        <Route path="/editDaftarKaryawan/:id" component={EditDaftarKaryawan} exact/>
        <Route path="/editDaftarSiswa/:id" component={EditDaftarSiswa} exact/>
        <Route path="/editDaftarGuru/:id" component={EditDaftarGuru} exact/>
        <Route path="/editProfil" component={EditProfil} exact/>
        <Route path="/statusPeriksa" component={StatusPeriksa} exact/>
        <Route path="/profile" component={Profile} exact/>
        <Route path="/tangani" component={Tangani} exact/>
      </Switch>
      
      </BrowserRouter>

      
    </div>
  );
}

export default App;
